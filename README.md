# SDNSEC
This repository contains applications for [Ryu Controller](https://osrg.github.io/ryu/) that allow for seamless integration of SDN functionalities into the security elements of the network. The work is based on two Ryu Applications:

* [REST Firewall](https://github.com/osrg/ryu/blob/master/ryu/app/rest_firewall.py)
* [REST QoS](https://github.com/osrg/ryu/blob/master/ryu/app/rest_qos.py)

and enables the communication infrastructure to harness the functionalities of the OpenFlow switch (typically [Open vSwitch](https://www.openvswitch.org/)) and employ it as both dynamic content-driven firewall and QoS Metering and enforcement tool.

## Requirements
In order to work properly, this package needs the following requirements to be met on the controller server:

* Python 3.6+,
* Ryu 4.30+.

And in the communication infrastructure (the switches):

* OpenFlow 1.3 Support.


## Basic Usage

The source code contains two applications for Ryu SDN controller. These also include a WSGI server handling the REST HTTP requests as specified in the source code.
The controller instructs the OpenFlow switches to work as the firewall or QoS enforcement tool using the standard OpenFlow entries in the Flow tables of the switch.

To run the controller application, simply run:


```bash
ryu-manager restfw.py
```

or

```bash
ryu-manager restq.py
```
**Important:** Both applications need to be run as root user.

When the application is started a standard OpenFlow discovery procedure takes place between the controller and the OpenFlow switches. The switches that are configured to connect to the given controller (via specifying the controller IP and port) will then join the firewall network and the controller can install the Flow entries based on the information it receives via the REST communication. The same applies for the QoS enforcememt tool.

The Flow entries can specify the Flow match based on the following OpenFlow Match Fields [OpenFlow Specs](https://www.opennetworking.org/wp-content/uploads/2014/10/openflow-spec-v1.3.0.pdf):

* "priority": "0 to 65533"
* "in_port" : "<int>"
* "dl_src"  : "<xx:xx:xx:xx:xx:xx>"
* "dl_dst"  : "<xx:xx:xx:xx:xx:xx>"
* "dl_type" : "<ARP or IPv4 or IPv6>"
* "nw_src"  : "<A.B.C.D/M>"
* "nw_dst"  : "<A.B.C.D/M>"
* "ipv6_src": "<xxxx:xxxx:xxxx:xxxx:xxxx:xxxx:xxxx:xxxx/M>"
* "ipv6_dst": "<xxxx:xxxx:xxxx:xxxx:xxxx:xxxx:xxxx:xxxx/M>"
* "nw_proto": "<TCP or UDP or ICMP or ICMPv6>"
* "tp_src"  : "<int>"
* "tp_dst"  : "<int>"
* "actions" : "<ALLOW or DENY>"

The Flow entry can be added to the switch based on the REST request, such as:

```bash
curl -X POST -d '{"nw_src": "10.0.0.1/32", "nw_dst": "10.0.0.2/32", "nw_proto": "ICMP"}' http://localhost:8080/firewall/rules/{switch-id}
```

The currently installed rules can be viewed by invoking:

```bash
curl http://localhost:8080/firewall/rules/{switch-id}
```

This lists all the rules together with their respective ids that can be used to delete the rule as needed with following curl command:

```bash
curl -X DELETE -d '{"rule_id": "5"}' http://localhost:8080/firewall/rules/{switch-id}
```

**Important:** The switch has no automatic learning capabilities meaning that all the PacketIn messages from the switch to controller are discarded. The appropriate log entry is created for each PacketIn message.
